FROM amarburg/trisect:runtime

ENV DEBIAN_FRONTEND=noninteractive

ARG WORKSPACE=/workspace
ARG CATKIN_WS=${WORKSPACE}/trisect_ws

#
# dependencies for the Trisect software stack (many!)
#
WORKDIR /tmp
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
            apt-utils \
            && \
    rm -rf /var/lib/apt/lists/*

WORKDIR ${CATKIN_WS}/src/trisect_sw
COPY . ${CATKIN_WS}/src/trisect_sw

WORKDIR ${CATKIN_WS}/src
RUN vcs import < trisect_sw/trisect.rosinstall

## Whoops
WORKDIR ${CATKIN_WS}/src/libtrisect
RUN git submodule init && git submodule update

WORKDIR ${CATKIN_WS}
RUN /ros_entrypoint.sh catkin config --blacklist darknet_ros
RUN /ros_entrypoint.sh catkin build

RUN echo "source ${CATKIN_WS}/devel/setup.bash" > ~/.bashrc


# install ROS packages
#
# RUN apt-get update && \
#     apt-get install -y --no-install-recommends \
# 		ros-melodic-`echo "${ROS_PKG}" | tr '_' '-'` \
# 		ros-melodic-image-transport \
# 		ros-melodic-vision-msgs \
#           python-rosdep \
#           python-rosinstall \
#           python-rosinstall-generator \
#           python-wstool \
#     && rm -rf /var/lib/apt/lists/*


#
# init/update rosdep
#
# RUN apt-get update && \
#     cd ${ROS_ROOT} && \
#     rosdep init && \
#     rosdep update && \
#     rm -rf /var/lib/apt/lists/*


#
# setup entrypoint
#


# ENTRYPOINT ["/ros_entrypoint.sh"]
# CMD ["bash"]
# WORKDIR /
