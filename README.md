# trisect

This is the top-level meta-package for Trisect perception sensor.

# Branches:

* `trisect-5.1.4/release-2.x` is the latest **alpha** version under Jetpack 5.1.4 (["Bagsful/Crowd"](https://trisect-perception-sensor.gitlab.io/trisect-docs/docs/versions/))
* `trisect-5.1.4/release-1.x` is the previous working version under Jetpack 5.1.4 (["Bagsful/Crowd"](https://trisect-perception-sensor.gitlab.io/trisect-docs/docs/versions/))
* `trisect-4.5.1` is the original stable version under Jetpack 4.5.1 (["Amigos"](https://trisect-perception-sensor.gitlab.io/trisect-docs/docs/versions/))

See [release notes for more details](https://trisect-perception-sensor.gitlab.io/trisect-docs/docs/versions/).

# Running

To run the default Visionworks-based stereo matcher with the "full Trisect stack":

```
roslaunch trisect_sw trisect_nodelet.launch
```

To run the default VPI-based stereo matcher:

```
roslaunch trisect_sw trisect_nodelet.launch stereo_matcher:=vpi
```

Note the version of Jetpack on Trisect provides VPI 0.3, which is ... not very good.

# Launchfiles

The default launchfile is `trisect_nodelet.launch`, which will calculate a stereo point cloud.

The launchfile will start a `web_video_server` on port 8080.

Here are the critical parameters:

* `do_stereo` (`true` or `false`).  If `true`, the stereo pipeline will run.  Defaults to `true`
* `environment`   Calibration files are loaded from the directory "<hostname>/<environment>/" in this repository.
* `mono_bit_depth` gives the bit depth of the monocular (stereo) cameras.  Value values: 8, 10, and 12 bit.  Default: 8
* `stereo_scale` gives the downsampling of the mono images before stereo.  Default: 4

See the [launchfile](launch/trisect_nodelet.launch) for a full list of parameters.

As of 3/2024 the standard Trisect topic names have been adjusted slightly to regluarize across different OPAL projects.  If `ros_topics_version:=v1`, the old topic names will be used.


# Building

The `trisect.rosinstall` includes both the software developed specifically for the platform as well as additional (non-ROS-standard) packages used on the platform.

```
mkdir -p trisect_ws/src/
cd trisect_ws/src
git clone https://gitlab.com/rsa-perception-sensor/trisect_sw
vcs import < trisect_sw/trisect.repos
<some other stuff I'm sure I've missed>
```


# Building in Docker

The [`Dockerfile`](Dockerfile) builds the `trisect_sw` software stack inside the [`trisect_docker`](https://gitlab.com/rsa-perception-sensor/trisect_docker) base image.  To build a Docker image with the Trisect sw:

```
docker build -t trisect_sw:latest .
```

You **must** be on a Jetson platform with `nvidia-docker`.   The [`visionworks.csv`]() file from `trisect_docker` must be installed in `/etc/


# License

This repo is released under the [BSD 3-Clause License](LICENSE).
